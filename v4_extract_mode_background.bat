@call v4_base.bat

python v4_bgext.py^
 --video %dip_video%^
 --width %dip_width%^
 --auto-screenshots %dip_autoscreenshots%^
 --close-frame %dip_bg_end%^
 --mode sliding^
 --video-out %dip_video_out%^
 --bg-start-frame %dip_bg_begin%^
 --bg-stop-frame %dip_bg_end%^
 --background %dip_bg_dir%\mode.jpg^
 --history-size 160