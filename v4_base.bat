@set PYTHONPATH=c:\dip\

@set dip_width=480
@set dip_data_dir=output\data
@set dip_data=%dip_data_dir$
@set dip_video_out_dir=output\video\
@set dip_video_out=%dip_video_out_dir%result.avi
@set dip_bg_dir_base=output\background
@set dip_bg_dir=%dip_bg_dir_base%

goto white_mouse_2

:white_mouse_1
@set dip_video=video\white1.mp4
@set dip_autoscreenshots=150 220 330 400 520 680
@set dip_stop_frame=681
@set dip_bg_begin=0
@set dip_bg_end=149
@set dip_gt=ground_truth\white1
@set dip_data=%dip_data_dir%\white1
@set dip_bg_dir=%dip_bg_dir_base%\white1
@set dip_ccx=233
@set dip_ccy=134
@set dip_ccr=27
@set dip_ccr1=69
@set dip_ccr2=115
goto skip

:white_mouse_2
@set dip_video=video\white2.mp4
@set dip_autoscreenshots=590 666 750 1000 1100 1200
@set dip_stop_frame=1201
@set dip_bg_begin=275
@set dip_bg_end=400
@set dip_gt=ground_truth\white2
@set dip_data=%dip_data_dir%\white2
@set dip_bg_dir=%dip_bg_dir_base%\white2
@set dip_ccx=246
@set dip_ccy=245
@set dip_ccr=40
@set dip_ccr1=104
@set dip_ccr2=170
goto skip

:black_mouse_1
@set dip_video=video\black1.mts
@set dip_autoscreenshots=500 550 650 800 1000 1250
@set dip_stop_frame=1251
@set dip_bg_begin=280
@set dip_bg_end=430
@set dip_gt=ground_truth\black1
@set dip_data=%dip_data_dir%\black1
@set dip_bg_dir=%dip_bg_dir_base%\black1
goto skip

:skip