from dip_bgextraction import *


# Простое вычитание фона (полноцветное, среднее арифметическое для N кадров)


class SimpleBackground(Background):
    def __init__(self, bg):
        self.image = bg

    def subtract(self, frame, images):
        frame_image_flt = frame.build_flt(lazy=True)
        bg_image_flt = self.image.build_flt(lazy=True)

        result_flt = Image.diff_flt(frame_image_flt, bg_image_flt)

        result = Image.cvt_to_grey(Image.cvt_from_float(result_flt))
        return Image.threshold(result, get_args().threshold)


class SimpleBackgroundExtraction(IBackgroundExtractionMethod):
    def __init__(self):
        self.frames_handled = 0
        self.bg_image_flt = None

    def handle_another_frame(self, images):
        if not get_args().should_load_bg:
            frame_image_flt = images.original.build_flt(lazy=True)
            self.bg_image_flt = Image.sum_flt(self.bg_image_flt, frame_image_flt)
            self.frames_handled += 1

    def extract_background(self, images):
        bg = images.background
        if get_args().should_load_bg:
            bg.set_image(cv2.imread(get_args().path_bg))
        else:
            bg.image_flt = Image.div_flt(self.bg_image_flt, self.frames_handled)
            bg.build_image_from_flt()
        return SimpleBackground(bg)
