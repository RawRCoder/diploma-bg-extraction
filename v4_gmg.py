from dip_lifecycle import *
import cv2


class GMGLifeCycle(LifeCycle):
    def __init__(self):
        LifeCycle.__init__(self)
        self.fgbg = cv2.bgsegm.createBackgroundSubtractorGMG()
        self.kernel = np.ones((5, 5), dtype='uint8')

    def get_method_name(self):
        return 'gmg'

    def handle_frame(self):

        fgmask = self.fgbg.apply(self.images.original.image)

        fgmask = cv2.morphologyEx(fgmask, cv2.MORPH_CLOSE, self.kernel, iterations=1)

        self.images.result.set_image(fgmask)
        super().handle_frame()


if __name__ == '__main__':
    main = GMGLifeCycle()
    main.loop()
