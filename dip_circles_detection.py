import cv2
from scipy import stats
import numpy as np

from dip_args import *
from dip_image import *


def are_not_far_away(a, b, max_dist=35):
    diff = np.subtract(a, b)
    return diff[0]**2+diff[1]**2 <= max_dist


class Circle:
    def __init__(self, center, r):
        self.center = center
        self.radius = r

    def draw(self, target, color=(0,0,255), thickness=1):
        cv2.circle(target, self.center, self.radius, color, thickness)


class CirclesDetector:
    def __init__(self):
        self.circles = [None, None, None]
        self._found = []

    def update(self):
        center = get_args().ccc
        radius = get_args().ccr
        radius1 = get_args().ccr1
        radius2 = get_args().ccr2
        self.circles[0] = Circle(center, radius)
        self.circles[1] = Circle(center, radius1)
        self.circles[2] = Circle(center, radius2)
