from dip_bgextraction import *


# Квадрат отношения разницы фона и кадра к отклонению в точке (полноцветное, среднее арифметическое для N кадров)


class SquaredNormalizedDiffBackground(Background):
    def __init__(self, bg, deviation):
        self.image = bg
        self.deviation = deviation

    def subtract(self, frame, images):
        frame_image_flt = frame.build_flt(lazy=True)
        bg_image_flt = self.image.build_flt(lazy=True)
        bg_deviation_flt = self.deviation.build_flt(lazy=True)

        result1_flt = Image.sub_flt(frame_image_flt, bg_image_flt)
        result2_flt = Image.div_flt(result1_flt, bg_deviation_flt)
        result3_flt = Image.squared_flt(result2_flt) / (get_args().k)
        result = Image.cvt_from_float(result3_flt)
        images.result_fc.image = result

        result_grey = Image.cvt_to_grey(result)
        images.result_g.image_grey = result_grey
        images.result_g.build_image_from_grey()
        return Image.threshold(result_grey, get_args().threshold)


class NormalizedDiffBackgroundExtraction(IBackgroundExtractionMethod):
    def __init__(self):
        self.frames_handled = 0
        self.bg_image_flt = None
        self.min_bg_image = None
        self.max_bg_image = None
        self.min_deviation = None

    def handle_another_frame(self, images):
        if get_args().should_load_bg:
            if images.background.image is None:
                images.background.set_image(cv2.imread(get_args().path_bg))
        else:
            frame_image_flt = images.original.build_flt(lazy=True)
            self.bg_image_flt = Image.sum_flt(self.bg_image_flt, frame_image_flt)

        self.min_bg_image = Image.min(self.min_bg_image, images.original.image)
        self.max_bg_image = Image.max(self.max_bg_image, images.original.image)
        self.frames_handled += 1

    def extract_background(self, images):
        bg = images.background
        imin = images.min_background
        imax = images.max_background
        deviation = images.background_deviation

        if not get_args().should_load_bg:
            bg.image_flt = Image.div_flt(self.bg_image_flt, self.frames_handled)
            bg.build_image_from_flt()

        imin.image = self.min_bg_image
        imax.image = self.max_bg_image

        deviation.image = Image.avg(
                Image.sub(bg.image, imin.image),
                Image.sub(imax.image, bg.image)
            )

        temp = Image.mul_flt(deviation.build_flt(), 1.2)
        if self.min_deviation is None:
            self.min_deviation = np.ones(np.shape(temp), float) * (5/255.0)
        deviation.image_flt = self.min_deviation + temp

        return SquaredNormalizedDiffBackground(bg, deviation)