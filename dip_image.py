import cv2
import numpy as np
import imutils
import time
from scipy import stats


class Image:
    @staticmethod
    def cvt_to_grey(image):
        if len(np.shape(image)) == 3:
            return cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        return image.copy()

    @staticmethod
    def cvt_to_bgr(image):
        return cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)

    @staticmethod
    def cvt_to_float(image):
        return np.float32(image) / 255.0

    @staticmethod
    def cvt_to_grey_float(image):
        return Image.cvt_to_float(Image.cvt_to_grey(image))

    @staticmethod
    def cvt_from_float(image):
        return cv2.convertScaleAbs(image * 255)

    @staticmethod
    def mode(images):
        t = time.time()
        a = stats.mode(images)
        print("Mode {} for {} sec".format(np.shape(images), time.time() - t))
        return a

    @staticmethod
    def mul_flt(a, b):
        return np.multiply(a, b)

    @staticmethod
    def sum_flt(a, b):
        if a is None:
            return b.copy()
        if b is None:
            return a.copy()
        return np.add(a, b)

    @staticmethod
    def min(a, b):
        if a is None:
            return b.copy()
        if b is None:
            return a.copy()
        return cv2.min(a, b)

    @staticmethod
    def max(a, b):
        if a is None:
            return b.copy()
        if b is None:
            return a.copy()
        return cv2.max(a, b)

    @staticmethod
    def div_flt(a, b):
        return np.divide(a, b)

    @staticmethod
    def sub_flt(a, b):
        return np.subtract(a, b)

    @staticmethod
    def sub(a, b):
        return cv2.subtract(a, b)

    @staticmethod
    def diff_flt(a, b):
        return np.abs(np.subtract(a, b))

    @staticmethod
    def squared_flt(a):
        return np.power(a, 2)

    @staticmethod
    def resize(img, w=None, h=None):
        return imutils.resize(img, width=w, height=h)

    @staticmethod
    def threshold(image, min_value):
        if min_value == 'otsu':
            return Image.threshold_otsu(image)
        _, r = cv2.threshold(image, min_value, 255, cv2.THRESH_BINARY)
        return r

    @staticmethod
    def threshold_otsu(image):
        otsu_threshold, r = cv2.threshold(image, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
        print('Otsu: {}'.format(otsu_threshold))
        return r

    @staticmethod
    def avg(a, b):
        return cv2.addWeighted(a, 0.5, b, 0.5, 1.0)

    @staticmethod
    def pad_zeroes(image, size):
        return np.pad(image, size, mode='constant')

    @staticmethod
    def get_conv_batch(image, x, y, hsz):
        return image[x - hsz:x + hsz + 1, y - hsz:y + hsz + 1]

    @staticmethod
    def generate_batches(image, sz, already_padded=False):
        f = Image.generate_batches_padded if already_padded else Image.generate_batches_unpadded
        for a in f:
            yield a

    @staticmethod
    def generate_batches_padded(image, sz):
        size = np.shape(image)
        size = (size[0] - sz+1, size[1] - sz+1)
        hsz = int((sz-1)/2)
        for x in range(0, size[0]):
            for y in range(0, size[1]):
                yield (x, y, Image.get_conv_batch(image, x+hsz, y+hsz, hsz))

    @staticmethod
    def generate_batches_unpadded(image, sz):
        hsz = int((sz-1)/2)
        padded = Image.pad_zeroes(image, hsz)
        for a in Image.generate_batches_padded(padded, sz):
            yield a

    @staticmethod
    def get_contour_center(contour):
        moments = cv2.moments(contour)
        if moments['m00'] != 0:
            return int(moments['m10'] / moments['m00']), int(moments['m01'] / moments['m00'])

    @staticmethod
    def find_single_object(image):
        se = np.ones((5, 5), dtype='uint8')
        image = cv2.morphologyEx(image, cv2.MORPH_CLOSE, se)

        _, contours, _ = cv2.findContours(image, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
        zeros = np.zeros(np.shape(image), np.uint8)
        if contours is None or len(contours) < 1:
            return zeros, (None, None), None
        max_id, max_area = max(enumerate(contours), key=lambda cp:cv2.contourArea(cp[1]))
        contour = contours[max_id]
        cv2.drawContours(zeros, [contour], 0, 255, -1)
        return zeros, Image.get_contour_center(contour), contour

    def __init__(self, title):
        self.title = title or "Unnamed image"
        self.image = None
        self.image_flt = None
        self.image_grey = None
        self.image_grey_flt = None

    def should_display(self):
        return self.image is not None

    def display(self):
        cv2.imshow(self.title, self.image)

    def width(self):
        return np.shape(self.image)[0]

    def height(self):
        return np.shape(self.image)[1]

    def set_image_from_denormalized(self, image):
        self.image = Image.cvt_from_float(image)

    def build_flt(self, lazy=False):
        if not lazy or self.image_flt is None:
            self.image_flt = Image.cvt_to_float(self.image)
        return self.image_flt

    def build_grey(self, lazy=False):
        if not lazy or self.image_grey is None:
            self.image_grey = Image.cvt_to_grey(self.image)
        return self.image_grey

    def build_grey_flt(self, lazy=False):
        if not lazy or self.image_grey_flt is None:
            grey = self.image
            if len(np.shape(grey)) > 2:
                grey = self.build_grey(lazy=True)
            else:
                self.image_grey = grey
            self.image_grey_flt = Image.cvt_to_float(grey)
        return self.image_grey_flt

    def build_image_from_flt(self):
        self.set_image_from_denormalized(self.image_flt)

    def build_image_from_grey(self):
        self.image = Image.cvt_to_bgr(self.image_grey)

    def set_image(self, image):
        self.image = image
        self.image_flt = None
        self.image_grey = None
        self.image_grey_flt = None


class ImageContainer:
    original = Image("Original")

    background = Image("Average BG")
    min_background = Image("Min BG")
    max_background = Image("Max BG")
    background_deviation = Image("BG deviation")
    edge = Image("Edge")

    result = Image("Result")
    result_fc = Image("Result (Full Color)")
    result_g = Image("Result (Grey)")
    result_marked = Image("Result (Marked)")

    def get_images(self):
        yield self.original
        #yield self.min_background
        #yield self.max_background
        yield self.background
        yield self.background_deviation
        yield self.result
        yield self.result_fc
        yield self.result_g
        yield self.edge
        yield self.result_marked
