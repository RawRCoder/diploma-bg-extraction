from dip_bgextraction import *
from dip_bgext_simple import *
import queue


# Вычитание усредненного за последние N кадров фона


class SlidingWindowBackgroundExtraction(IBackgroundExtractionMethod):
    def __init__(self):
        self.image_queue = queue.deque()

    def handle_another_frame(self, images):
        image = images.original.image
        self.image_queue.append(image.copy())
        if len(self.image_queue) > get_args().history_size:
            self.image_queue.popleft()

    def get_bg(self, images):
        bg = images.background
        window = np.array(self.image_queue)
        result, _ = Image.mode(window)

        bg.image = result[0]
        return bg

    def extract_background(self, images):
        return SimpleBackground(self.get_bg(images))
