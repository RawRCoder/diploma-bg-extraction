@call v4_base.bat

python v4_gmg.py^
 --video %dip_video%^
 --width %dip_width%^
 --auto-screenshots %dip_autoscreenshots%^
 --data %dip_data%^
 --video-out %dip_video_out%^
 --close-frame %dip_stop_frame%^
 --ground-truth %dip_gt%^
 --background %dip_bg_dir%\mog2.jpg^
 --circles 1^
 --ccx %dip_ccx%^
 --ccy %dip_ccy%^
 --ccr %dip_ccr%^
 --ccr1 %dip_ccr1%^
 --ccr2 %dip_ccr2%^
 --find-single-object 1