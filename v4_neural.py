from dip_lifecycle import LifeCycle
from dip_args import get_args
from dip_image import *
from keras.layers import Conv2D, Activation, MaxPooling2D, Flatten, Dense, InputLayer, Dropout
from keras.optimizers import RMSprop
from keras.models import Sequential
import os


T = 27


def construct_cnn():
    model = Sequential()
    model.add(InputLayer(input_shape=(T, T, 2)))

    model.add(Conv2D(6, (5, 5), padding='same'))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(3, 3), strides=(3, 3)))

    model.add(Conv2D(16, (5, 5), padding='same'))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(3, 3), strides=(3, 3)))

    model.add(Dropout(0.2)) # ??

    model.add(Flatten())
    model.add(Dense(120))
    model.add(Activation('relu')) # or softmax?

    model.add(Dense(1))
    model.add(Activation('softmax'))

    model.summary()

    optimizer = RMSprop(lr=0.001, rho=0.9, epsilon=1e-08, decay=0.0)

    model.compile(optimizer=optimizer, loss='binary_crossentropy', metrics=['accuracy'])
    return model


def enumrate_data(path):
    id = 0
    while True:
        suffix = "_" + str(id)+".jpg"
        fname_o = os.path.join(path, 'original'+suffix)
        fname_gt = os.path.join(path, 'ground_truth'+suffix)
        if os.path.isfile(fname_o) and os.path.isfile(fname_gt):
            try:
                img_o = cv2.imread(fname_o)
            except:
                break

            try:
                img_gt = cv2.imread(fname_gt)
            except:
                break

            print('Loaded {} and {} ...'.format(fname_o, fname_gt))

            o_flt = Image.cvt_to_grey_float(img_o)
            gt_flt = Image.cvt_to_grey_float(img_gt)
            yield o_flt, gt_flt
            id += 1
        else:
            break


def fit_data_generator(path_dataset, background):
    hsz = int((T-1)/2)
    pbg = Image.pad_zeroes(background, hsz)
    for (original, ground_truth) in enumrate_data(path_dataset):
        psrc = Image.pad_zeroes(original, hsz)
        combined_original = np.transpose([psrc, pbg], (1, 2, 0))
        for (x, y, dataX) in Image.generate_batches_padded(combined_original, T):
            dataY = ground_truth[x, y]
            #if ((x % 150) == 0) and ((y % 150) == 0):
            #    print("Extracting batch {}, {} ...".format(x, y))
            yield (dataX, dataY)


def fit_data_generator_batched(path_dataset, background, batch_size=100):
    batchX = []
    batchY = []

    for (X, Y) in fit_data_generator(path_dataset, background):
        batchX.append(X)
        batchY.append(Y)
        if len(batchX) == batch_size:
            rX = np.reshape(batchX, np.shape(batchX))
            rY = np.reshape(batchY, np.shape(batchY))

            yield rX, rY
            batchX.clear()
            batchY.clear()

    if len(batchX) > 0:
        rX = np.reshape(batchX, np.shape(batchX))
        rY = np.reshape(batchY, np.shape(batchY))
        yield rX, rY


class NeuralLifeCycle(LifeCycle):
    def __init__(self):
        LifeCycle.__init__(self)
        self.model = construct_cnn()

        path_dataset = get_args().path_dataset
        path_bg = get_args().path_bg
        print('Loading data ...')
        self.images.background.image = cv2.imread(path_bg)
        bg_flt = self.images.background.build_grey_flt()
        W = np.shape(bg_flt)[0]
        H = np.shape(bg_flt)[1]
        N = 10

        print('Learning ...')
        self.model.fit_generator(fit_data_generator_batched(path_dataset, bg_flt, batch_size=100), 100, epochs=5)

    def handle_frame(self):
        super().handle_frame()


if __name__ == '__main__':
    main = NeuralLifeCycle()
    main.loop()