import cv2
import numpy as np
from dip_image import *
from dip_args import *


class Background:
    image = None

    def save_to_file(self, filename):
        cv2.imwrite(filename, self.image.image)

    def subtract(self, frame, images):
        return None


class IBackgroundExtractionMethod:
    def handle_another_frame(self, images):
        pass

    def extract_background(self, images):
        pass
