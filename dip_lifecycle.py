import sys

from dip_args import *
from dip_circles_detection import CirclesDetector
from dip_mouse_events import MouseTracker
from dip_precision import PrecisionCalculator
from imutils.video import FileVideoStream
from dip_image import *
import cv2
import time
import os
from scipy import stats


class LifeCycle:
    def __init__(self):
        self.video = FileVideoStream(get_args().path_video).start() # cv2.VideoCapture(get_args().path_video)
        self.video_width = int(self.video.stream.get(cv2.CAP_PROP_FRAME_WIDTH))
        self.video_height = int(self.video.stream.get(cv2.CAP_PROP_FRAME_HEIGHT))
        self.video_out = None
        if get_args().path_video_out is not None:
            self.video_out = cv2.VideoWriter(get_args().path_video_out, cv2.VideoWriter_fourcc(*'MJPG'), 25, (1280, 720))

        time.sleep(1.0)
        self.fps = get_args().fps or 30
        self.images = ImageContainer()
        self.should_stop = False
        self.paused = False
        self.frame_id = None
        self.saved_counter = 0

        self.precision_calc = PrecisionCalculator()
        self.circle_detector = CirclesDetector()

        self.winname = "Settings"
        self.mtracker = MouseTracker()

    def onCCXChanged(self, v):
        args = get_args()
        args.ccc = (v, args.ccc[1])

    def onCCYChanged(self, v):
        args = get_args()
        args.ccc = (args.ccc[0], v)

    def onCCRChanged(self, v):
        args = get_args()
        args.ccr = v

    def onCCR1Changed(self, v):
        args = get_args()
        args.ccr1 = v

    def onCCR2Changed(self, v):
        args = get_args()
        args.ccr2 = v

    def onThresholdChanged(self, v):
        args = get_args()
        args.threshold = v

    def get_method_name(self):
        return 'UNKNOWN'

    def loop(self):
        self.frame_id = 0
        frame = None
        while self.video.more() and not self.should_stop:
            t_start = time.time()
            if not self.paused:
                frame = self.video.read()
                frame = Image.resize(frame, get_args().resize_width, get_args().resize_height)

                self.images.original.set_image(frame)

                self.handle_frame()

            t_end = time.time()
            key_wait_time = max(1/self.fps - (t_end - t_start), 0.001)
            key = cv2.waitKey(int(key_wait_time*1000))
            byte_key = key & 0xFF

            if key != -1 and key != 255:
                self.handle_key_input(key, byte_key)

            if self.paused:
                self.images.original.set_image(frame.copy())
            #self.render_video()
            #edge = cv2.Canny(self.images.original.build_grey(lazy=True), get_args().canny_min, get_args().canny_max)
            #self.images.edge.set_image(edge)

            if self.mtracker.position is not None and self.mtracker.position[0] is not None:
                cv2.circle(self.images.original.image, self.mtracker.position, 3, (0, 255, 0), thickness=-1)

            if get_args().should_find_circles:
                self.circle_detector.update()
                for circle in self.circle_detector.circles:
                    if circle is not None:
                        circle.draw(self.images.original.image)

            text = "#{}".format(self.frame_id)
            cv2.putText(self.images.original.image, text, (0, 20), cv2.FONT_HERSHEY_PLAIN, 1.0, (255, 255, 255))

            for image in self.images.get_images():
                if image.should_display():
                    image.display()

            if not self.paused:
                self.frame_id += 1

        mt = self.mtracker
        if mt.position is not None:
            print("Задержка первого входа в центр: {} кадров".format(mt.P_ARRIVAL_LATENCY))
            print("Задержка первого выхода из центра: {} кадров".format(mt.P_EXIT_CENTER_LATENCY))
            print("Время пребывания в центральной части: {} кадров".format(mt.P_TIME_IN_CENTER))
            print("Количество входов в центральную часть: {} раз".format(mt.P_TIMES_ENTERED_CENTER))
            print("Количество выходов из центральной части: {} раз".format(mt.P_TIMES_EXITED_CENTER))
            print("Количество пересечений внутренней окружности: {} раз".format(mt.P_TIMES_CROSSED_EDGE[0]))
            print("Количество пересечений средней окружности: {} раз".format(mt.P_TIMES_CROSSED_EDGE[1]))
            print("Количество пересечений внешней окружности: {} раз".format(mt.P_TIMES_CROSSED_EDGE[2]))
        self._dispose()

    def render_video(self):
        newframe = None
        for image in self.images.get_images():
            if image.should_display():
                subframe = image.image.copy()
                text = "[{}]".format(image.title)
                cv2.putText(subframe, text, (0, 18), cv2.FONT_HERSHEY_PLAIN, 0.8, (255, 255, 255))

                if newframe is None:
                    newframe = subframe
                else:
                    newframe = np.concatenate((newframe, subframe))
        self.video_out.write(newframe)

    def handle_frame(self):
        if self.frame_id == 0:
            self.frame_shape = np.shape(self.images.original.image)
            shape = np.shape(self.images.original.image)
            print("Video dimensions: {} => {}".format((self.video_width, self.video_height), (shape[1], shape[0])))
            self.printed_video_dimensions = True

            cv2.namedWindow(self.winname)
            cv2.createTrackbar("Center X", self.winname, get_args().ccc[0], shape[0], lambda v: self.onCCXChanged(v))
            cv2.createTrackbar("Center Y", self.winname, get_args().ccc[1], shape[1], lambda v: self.onCCYChanged(v))
            minsz = np.min((shape[1], shape[0]))
            cv2.createTrackbar("Radius 0", self.winname, get_args().ccr, minsz, lambda v: self.onCCRChanged(v))
            cv2.createTrackbar("Radius 1", self.winname, get_args().ccr1, minsz, lambda v: self.onCCR1Changed(v))
            cv2.createTrackbar("Radius 2", self.winname, get_args().ccr2, minsz, lambda v: self.onCCR2Changed(v))
            if isinstance(get_args().threshold, int):
                cv2.createTrackbar("Threshold", self.winname, get_args().threshold, 255, lambda v: self.onThresholdChanged(v))

        if self.images.result.image is not None and get_args().find_single_object:
            self.images.result_marked.set_image(self.images.original.image.copy())
            mask, position, contour = Image.find_single_object(self.images.result.build_grey(lazy=True))
            self.images.result.set_image(mask)
            self.mtracker.update(position, self.frame_id)
            if self.mtracker.position is not None and self.mtracker.position[0] is not None:
                cv2.circle(self.images.result_marked.image, self.mtracker.position, 3, (0, 255, 0), thickness=-1)
                cv2.drawContours(self.images.result_marked.image, [contour], 0, (0, 255, 255), thickness=2)

        if get_args().auto_screenshots is not None:
            if self.images.result.image is not None:
                if self.frame_id in get_args().auto_screenshots:
                    path = get_args().path_dataset
                    fname_original = os.path.join(path, "source_{}.jpg".format(self.frame_id))
                    fname_result = os.path.join(path, "{}_result_{}.jpg".format(self.get_method_name(), self.frame_id))
                    cv2.imwrite(fname_original, self.images.original.image)
                    cv2.imwrite(fname_result, self.images.result.image)
                    if self.images.result_marked.image is not None:
                        fname = os.path.join(path, "{}_result_marked_{}.jpg"
                                             .format(self.get_method_name(), self.frame_id))
                        cv2.imwrite(fname, self.images.result_marked.image)
                    print("At frame #{}:".format(self.frame_id))
                    try:
                        self.precision_calc.analyze_frame(self.images.result.build_grey_flt(), self.frame_id)
                        print("\tTP = {}; FP = {}; TN = {}; FN = {};".format(
                            self.precision_calc.tp, self.precision_calc.fp,
                            self.precision_calc.tn, self.precision_calc.fn))
                        print("\tPre = {};".format(self.precision_calc.precision))
                        print("\tRe = {};".format(self.precision_calc.recall))
                        print("\tF-Measure = {}".format(self.precision_calc.fmeasure))
                    except Exception:
                        print("\tException: {}".format(sys.exc_info()[0]))
        if get_args().close_frame is not None and get_args().close_frame <= self.frame_id:
            self.should_stop = True

    def handle_key_input(self, key, byte_key):
        print('Debug: Key pressed: {} {}'.format(key, byte_key))
        if byte_key == 27:
            self.should_stop = True
        elif byte_key == 32:
            self.paused = not self.paused
        if byte_key == 115 and self.images.result.image is not None:
            suffix = str(self.saved_counter)+".jpg"
            path = get_args().path_dataset
            cv2.imwrite(os.path.join(path, "ground_truth_"+suffix), self.images.result.image)
            cv2.imwrite(os.path.join(path, "original_"+suffix), self.images.original.image)
            self.saved_counter += 1
        elif byte_key == 115:
            path = get_args().path_dataset
            cv2.imwrite(os.path.join(path, "original.jpg"), self.images.original.image)

    def _dispose(self):
        self.video_out.release()
        cv2.destroyAllWindows()
        self.video.stop()
