from dip_image import *
from dip_args import *
import os, os.path


class PrecisionCalculator:

    def __init__(self):
        self.tp = 0.0
        self.fp = 0.0

        self.tn = 0.0
        self.fn = 0.0

        self.precision = None
        self.recall =  None
        self.fmeasure = None

    def clear(self):
        self.tp = 0.0
        self.fp = 0.0

        self.tn = 0.0
        self.fn = 0.0

        self.precision = None
        self.recall = None
        self.fmeasure = None

    def analyze_frame(self, frame_th_flt, frame_id):
        self.clear()
        path = os.path.join(get_args().path_gt, "{}.jpg".format(frame_id))

        A = frame_th_flt
        GT = cv2.imread(path)
        if GT is None:
            print('No file named {} found'.format(path))
        try:
            GT = Image.cvt_to_grey(GT)
        except:
            pass
        GT = Image.cvt_to_float(GT)

        TP = self.tp = np.sum(np.multiply(A, GT))
        FP = self.fp = np.sum(np.multiply(A, 1 - GT))

        self.tn = np.sum(np.multiply(1 - A, 1 - GT))
        FN = self.fn = np.sum(np.multiply(1 - A, GT))

        Pr = self.precision = self._calc_precision(TP, FP)
        Re = self.recall = self._calc_recall(TP, FN)
        self.fmeasure = self._calc_fmeasure(Pr, Re)
        return self

    def _calc_precision(self, tp, fp):
        return tp / (tp + fp)

    def _calc_recall(self, tp, fn):
        return tp / (tp + fn)

    def _calc_fmeasure(self, pr, re):
        return 2 * pr*re / (pr + re)
