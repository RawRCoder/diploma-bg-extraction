from dip_lifecycle import *
import cv2


class MOG2LifeCycle(LifeCycle):
    def __init__(self):
        LifeCycle.__init__(self)
        self.fgbg = cv2.createBackgroundSubtractorMOG2()

    def get_method_name(self):
        return 'mog2'

    def handle_frame(self):
        fgmask = self.fgbg.apply(self.images.original.image)
        self.images.result.set_image(fgmask)
        super().handle_frame()


if __name__ == '__main__':
    main = MOG2LifeCycle()
    main.loop()
