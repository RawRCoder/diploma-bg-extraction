import argparse


def parse_as_nullable_int(s):
    try:
        return int(s)
    except TypeError:
        return None
    except ValueError:
        return None

def parse_as_nullable_float(s):
    try:
        return float(s)
    except TypeError:
        return None
    except ValueError:
        return None


class Args:
    def __init__(self):
        ap = argparse.ArgumentParser()
        ap.add_argument("-v", "--video", required=False, help="path to input video file", default="1.mp4")
        ap.add_argument("-o", "--video-out", required=False, help="path to output video file", default="result.avi")
        ap.add_argument("-b", "--background", required=False, help="path to I/O background", default="bg/bg.jpg")
        ap.add_argument("-d", "--data", required=False, help="path to I/O training data", default="data")
        ap.add_argument("-m", "--mode", required=False, help="background extraction mode", default="simple")
        ap.add_argument("-w", "--width", required=False, help="resize.width", default="600")
        ap.add_argument("--height", required=False, help="resize.height", default=None)
        ap.add_argument("--bg-start-frame", required=False, help="first frame to use for BG extraction", default=None)
        ap.add_argument("--bg-stop-frame", required=False, help="last frame to use for BG extraction", default=None)
        ap.add_argument("--threshold", required=False, help="subtraction threshold", default=None)
        ap.add_argument("--fps", required=False, help="FPS", default=None)
        ap.add_argument("--k", required=False, help="multiplier for ((f-BG)/dev)^2", default=None)
        ap.add_argument("--history-size", required=False,
                        help="max frames used for background extraction in sliding method",
                        default=None)
        ap.add_argument("--auto-screenshots", required=False,
                        help="frame ids to make screenshots", nargs='+', type=int)
        ap.add_argument("--close-frame", required=False, help="frame index to close app", default=None, type=int)
        ap.add_argument("--circles", required=False, help="should find circles on source image",
                        default=False, type=bool)
        ap.add_argument("--ground-truth", required=False, help="path to ground truth images", default="ground_truth")
        ap.add_argument("--ccx", required=False, help="center of a central circle", default=245)
        ap.add_argument("--ccy", required=False, help="center of a central circle", default=245)
        ap.add_argument("--ccr", required=False, help="radius of a central circle", default=40)
        ap.add_argument("--ccr1", required=False, help="radius of a central circle", default=89)
        ap.add_argument("--ccr2", required=False, help="radius of a central circle", default=200)
        ap.add_argument("--find-single-object", required=False, help="should find single object in result image",
                        default=False, type=bool)
        ap.add_argument("--should-load-bg", required=False, help="should not extract but load the BG",
                        default=False, type=bool)
        a = ap.parse_args()

        self.path_video = a.video
        self.path_bg = a.background
        self.path_dataset = a.data
        self.bg_extraction_mode = a.mode
        self.resize_width = parse_as_nullable_int(a.width)
        self.resize_height = parse_as_nullable_int(a.height)
        self.bg_start_frame = parse_as_nullable_int(a.bg_start_frame)
        self.bg_stop_frame = parse_as_nullable_int(a.bg_stop_frame)
        self.threshold = 'otsu' if a.threshold == 'otsu' else parse_as_nullable_int(a.threshold) or 20
        self.fps = parse_as_nullable_int(a.fps) or 30
        self.k = parse_as_nullable_float(a.k) or 250
        self.history_size = parse_as_nullable_int(a.history_size) or 300
        self.path_video_out = a.video_out
        self.auto_screenshots = a.auto_screenshots
        self.close_frame = a.close_frame
        self.path_gt = a.ground_truth
        self.should_find_circles = a.circles
        self.ccc = (int(a.ccx), int(a.ccy))
        self.ccr = int(a.ccr)
        self.ccr1 = int(a.ccr1)
        self.ccr2 = int(a.ccr2)
        self.path_gt = a.ground_truth
        self.find_single_object = a.find_single_object
        self.should_load_bg = a.should_load_bg

args = None


def get_args():
    global args
    if args is None:
        args = Args()
    return args