from dip_lifecycle import LifeCycle
from dip_bgext_simple import *
from dip_bgext_sqnorm import *
from dip_bgext_sliding import *
from dip_args import *


class BgExtLifeCycle(LifeCycle):
    def __init__(self):
        LifeCycle.__init__(self)

        self.bg = None

        bg_extractors = {
            'simple': SimpleBackgroundExtraction(),
            'sqnorm': NormalizedDiffBackgroundExtraction(),
            'sliding': SlidingWindowBackgroundExtraction()
        }
        bg_extractors['load'] = bg_extractors['simple']

        self.bg_extractor = bg_extractors[get_args().bg_extraction_mode]
        self.should_analyze_frame = False

    def handle_frame(self):
        if get_args().bg_start_frame == self.frame_id:
            self.should_analyze_frame = True

        if self.should_analyze_frame:
            self.bg_extractor.handle_another_frame(self.images)

        if self.bg is not None:
            result = self.bg.subtract(self.images.original, self.images)
            self.images.result.set_image(None)
            self.images.result.image_grey = result
            self.images.result.build_image_from_grey()

        if get_args().bg_stop_frame == self.frame_id:
            self.should_analyze_frame = False
            self.bg = self.bg_extractor.extract_background(self.images)
            if get_args().path_bg is not None:
                self.bg.save_to_file(get_args().path_bg)

        LifeCycle.handle_frame(self)

    def get_method_name(self):
        return 'bgext_'+get_args().bg_extraction_mode


if __name__ == '__main__':
    main = BgExtLifeCycle()
    main.loop()
