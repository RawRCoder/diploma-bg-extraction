from dip_args import *
import numpy as np
import math


class MouseTracker:
    def __init__(self):
        self.position = None
        self.sector = None
        self.edge = None

        self.sector_start_frame = None
        self.edge_start_frame = None

        self.frame_id = None

        self.P_ARRIVAL_LATENCY = None
        self.P_EXIT_CENTER_LATENCY = None
        self.P_TIME_IN_CENTER = 0
        self.P_TIMES_ENTERED_CENTER = 0
        self.P_TIMES_CROSSED_EDGE = [0, 0, 0]
        self.P_TIMES_EXITED_CENTER = 0

    def _center(self):
        return get_args().ccc

    def _dist_from_center(self, p):
        c = self._center()
        d = np.array(c) - np.array(p)
        return math.sqrt(d[0]**2 + d[1]**2)

    def _is_within(self, p, r):
        return self._dist_from_center(p) < r

    def _is_within_r0(self, p):
        return self._is_within(p, get_args().ccr)

    def _is_within_r1(self, p):
        return self._is_within(p, get_args().ccr1)

    def _is_within_r2(self, p):
        return self._is_within(p, get_args().ccr2)

    def _is_on_edge(self, p, r, th=0.05):
        k = self._dist_from_center(p) / r
        return abs(1 - k) <= th

    def _is_on_edge0(self, p, th=0.05):
        return self._is_on_edge(p, get_args().ccr, th)

    def _is_on_edge1(self, p, th=0.05):
        return self._is_on_edge(p, get_args().ccr1, th)

    def _is_on_edge2(self, p, th=0.05):
        return self._is_on_edge(p, get_args().ccr2, th)

    def update(self, pos, frame_id):
        self.frame_id = frame_id
        if pos is None or pos[0] is None:
            return

        self.position = pos
        if self._is_within_r0(pos):
            sector = 0
        elif self._is_within_r1(pos):
            sector = 1
        elif self._is_within_r2(pos):
            sector = 2
        else:
            sector = None

        if self._is_on_edge0(pos):
            edge = 0
        elif self._is_on_edge1(pos):
            edge = 1
        elif self._is_on_edge2(pos):
            edge = 2
        else:
            edge = None

        if sector == 0 and self.P_ARRIVAL_LATENCY is None:
            self.P_ARRIVAL_LATENCY = self.frame_id

        sector_changed = self.sector != sector
        sector_became_none = sector is None and sector_changed
        sector_was_none = self.sector is None
        sector_became_not_none = sector is not None and sector_was_none

        edge_changed = self.edge != edge
        edge_became_none = edge is None and edge_changed
        edge_was_none = self.edge is None
        edge_became_not_none = edge is not None and edge_was_none

        if sector_changed:
            if sector_became_not_none:
                self.sector_start_frame = frame_id
                if sector == 0:
                    self.P_TIMES_ENTERED_CENTER += 1
            elif sector_became_none:
                if self.sector == 0:
                    self.P_TIME_IN_CENTER += frame_id - self.sector_start_frame
                    self.P_TIMES_EXITED_CENTER += 1
                elif self.sector == 2:
                    self.P_TIMES_CROSSED_EDGE[2] += 1
            else:
                if self.sector == 0:
                    self.P_TIMES_EXITED_CENTER += 1
                    if self.P_EXIT_CENTER_LATENCY is None:
                        self.P_EXIT_CENTER_LATENCY = frame_id
                    if sector == 1:
                        self.P_TIMES_CROSSED_EDGE[0] += 1
                    self.P_TIME_IN_CENTER += frame_id - self.sector_start_frame
                elif self.sector == 1:
                    if sector == 0:
                        self.P_TIMES_CROSSED_EDGE[0] += 1
                        self.P_TIMES_ENTERED_CENTER += 1
                    elif sector == 2:
                        self.P_TIMES_CROSSED_EDGE[1] += 1
                else:
                    if sector == 1:
                        self.P_TIMES_CROSSED_EDGE[1] += 1
                self.sector_start_frame = frame_id

        self.sector = sector
        self.edge = edge

    def print_event(self, msg):
        print("[event] #{}: {}".format(self.frame_id, msg))