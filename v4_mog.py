from dip_lifecycle import *
import cv2


class MOGLifeCycle(LifeCycle):
    def __init__(self):
        LifeCycle.__init__(self)
        self.fgbg = cv2.bgsegm.createBackgroundSubtractorMOG(history=25, nmixtures=5, backgroundRatio=0.0001)

    def get_method_name(self):
        return 'mog'

    def handle_frame(self):
        fgmask = self.fgbg.apply(self.images.original.image)
        self.images.result.set_image(fgmask)
        super().handle_frame()


if __name__ == '__main__':
    main = MOGLifeCycle()
    main.loop()
